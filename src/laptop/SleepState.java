package laptop;

public class SleepState implements LaptopState {
    @Override
    public void pressPower(Laptop laptop) {
        System.out.println("Waking up");
        laptop.setState(new OnState());
    }

    @Override
    public void holdPower(Laptop laptop) {
        System.out.println("Forced off");
        laptop.setState(new OffState());
    }

    @Override
    public void openLid(Laptop laptop) {
        System.out.println("Waking up");
        laptop.setState(new OnState());
    }

    @Override
    public void closeLid(Laptop laptop) {
        // nie zmienia stanu

    }

    @Override
    public void hibernate(Laptop laptop) {
        System.out.println("Hibernating");
        laptop.setState(new HibernatedState());
    }

    @Override
    public void printState(Laptop laptop) {
        System.out.println("Sleeping");
    }
}

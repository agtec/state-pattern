package laptop;

public class OffState implements LaptopState {
    @Override
    public void pressPower(Laptop laptop) {
        System.out.println("Power on");
        laptop.setState(new OnState());
    }

    @Override
    public void holdPower(Laptop laptop) {
        System.out.println("Power on");
        laptop.setState(new OnState());
    }

    @Override
    public void openLid(Laptop laptop) {
        // nie zmienia
    }

    @Override
    public void closeLid(Laptop laptop) {
        // nie zmienia
    }

    @Override
    public void hibernate(Laptop laptop) {
        // nie da się
    }

    @Override
    public void printState(Laptop laptop) {
        System.out.println("Off");
    }
}

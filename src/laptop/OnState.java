package laptop;

public class OnState implements LaptopState {

    @Override
    public void pressPower(Laptop laptop) {
        System.out.println("Going sleep");
        laptop.setState(new SleepState());
    }

    @Override
    public void holdPower(Laptop laptop) {
        System.out.println("Forced power off");
        laptop.setState(new OffState());
    }

    @Override
    public void openLid(Laptop laptop) {
        // nie zmienia stanu
    }

    @Override
    public void closeLid(Laptop laptop) {
        System.out.println("Going sleep");
        laptop.setState(new SleepState());
    }

    @Override
    public void hibernate(Laptop laptop) {
        System.out.println("Hibernating");
        laptop.setState(new HibernatedState());
    }

    @Override
    public void printState(Laptop laptop) {
        System.out.println("On");
    }


}

package laptop;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Laptop laptop = new Laptop();

        boolean display = true;

        Scanner sc = new Scanner(System.in);

        do {
            System.out.println("Choose option: ");
            System.out.println("1 - press power");
            System.out.println("2 - hold power");
            System.out.println("3 - close lid");
            System.out.println("4 - open lid");
            System.out.println("5 - hibernate");
            System.out.println("6 - print state");
            System.out.println("7 - exit");

            String choice = sc.nextLine();

            switch (choice) {
                case "1":
                    laptop.pressPower();
                    break;
                case "2":
                    laptop.holdPower();
                    break;
                case "3":
                    laptop.closeLid();
                case "4":
                    laptop.openLid();
                    break;
                case "5":
                    laptop.hibernate();
                    break;
                case "6":
                    laptop.printState();
                    break;
                case "7":
                    display = false;
                    break;
                default:
                    System.out.println("Ivalid choice, choose one again");
                    break;
            }

        } while (display);
    }
}

package laptop;

public class Laptop {

    private LaptopState state = new OffState();

    public LaptopState getState() {
        return state;
    }

    public void setState(LaptopState state) {
        this.state = state;
    }

    void pressPower() {
        state.pressPower(this);
    };

    void holdPower() {
        state.holdPower(this);
    };

    void openLid() {
        state.openLid(this);
    };

    void closeLid() {
        state.closeLid(this);
    };

    void hibernate() {
        state.hibernate(this);
    };

    void printState() {
        state.printState(this);
    };
}

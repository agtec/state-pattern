package laptop;

public class HibernatedState implements LaptopState {
    @Override
    public void pressPower(Laptop laptop) {
        System.out.println("Waking up");
        laptop.setState(new OnState());
    }

    @Override
    public void holdPower(Laptop laptop) {
        System.out.println("On");
        laptop.setState(new OffState());
    }

    @Override
    public void openLid(Laptop laptop) {
        // nie zmienia stanu
    }

    @Override
    public void closeLid(Laptop laptop) {
        // nie zmienia stanu
    }

    @Override
    public void hibernate(Laptop laptop) {
        // nie zmienia stanu
    }

    @Override
    public void printState(Laptop laptop) {
        System.out.println("Hibernated");
    }
}

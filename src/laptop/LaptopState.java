package laptop;

public interface LaptopState {

    void pressPower(Laptop laptop);

    void holdPower(Laptop laptop);

    void openLid(Laptop laptop);

    void closeLid(Laptop laptop);

    void hibernate(Laptop laptop);

    void printState(Laptop laptop);
}
